package com.ciandt.challengeandroid.worldwondersapp.rest.json;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rafael on 16/07/15.
 */
public class WorldWonderResponseJson {

    @SerializedName("data")
    private final List<WorldWonderJson> mWorldWonderJsons;

    public WorldWonderResponseJson(List<WorldWonderJson> worldWonderJsons) {
        mWorldWonderJsons = worldWonderJsons;
    }

    public List<WorldWonderJson> getWorldWonderJsons() {
        return mWorldWonderJsons;
    }
}