package com.ciandt.challengeandroid.worldwondersapp.ui.adapters.viewholders;

import com.ciandt.challengeandroid.worldwondersapp.R;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by rafael on 16/07/15.
 */
public class WorldWonderViewHolder extends RecyclerView.ViewHolder {

    public final TextView mName;

    public final TextView mCountry;

    public final ImageView mImageView;

    public final TextView mDescription;

    public WorldWonderViewHolder(View itemView) {
        super(itemView);
        mName = (TextView) itemView.findViewById(R.id.world_wonder_name);
        mCountry = (TextView) itemView.findViewById(R.id.world_wonder_country);
        mImageView = (ImageView) itemView.findViewById(R.id.world_wonder_image);
        mDescription = (TextView) itemView.findViewById(R.id.world_wonder_description);
    }
}
