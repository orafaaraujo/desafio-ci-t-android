package com.ciandt.challengeandroid.worldwondersapp.rest.json;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rafael on 16/07/15.
 */
public class LoginResponseJson {

    @SerializedName("codigo")
    private final int mCode;

    @SerializedName("mensagem")
    private final String mMessage;

    @SerializedName("dados")
    private final UserDataJson mData;

    public LoginResponseJson(int code, String message, UserDataJson data) {
        mCode = code;
        mMessage = message;
        mData = data;
    }

    public int getCode() {
        return mCode;
    }

    public String getMessage() {
        return mMessage;
    }

    public UserDataJson getData() {
        return mData;
    }
}