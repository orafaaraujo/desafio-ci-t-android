package com.ciandt.challengeandroid.worldwondersapp.policy;

import android.text.TextUtils;

/**
 * Created by rafael on 16/07/15.
 */
public class EmailPolicy {

    public static boolean isValid(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}