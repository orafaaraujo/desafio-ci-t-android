package com.ciandt.challengeandroid.worldwondersapp.policy;

import android.text.TextUtils;

/**
 * Created by rafael on 16/07/15.
 */
public class PasswordPolicy {

    public static boolean isValid(String password) {
        return !TextUtils.isEmpty(password);
    }
}