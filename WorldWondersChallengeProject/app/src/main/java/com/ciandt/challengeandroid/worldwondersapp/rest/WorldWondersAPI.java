package com.ciandt.challengeandroid.worldwondersapp.rest;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.ciandt.challengeandroid.worldwondersapp.BuildConfig;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by rafael on 16/07/15.
 */
public class WorldWondersAPI {

    public static WorldWondersInterface getServices() {
        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(BuildConfig.ENDPOINT)
                .setConverter(new GsonConverter(getGson()))
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE).build();
        return adapter.create(WorldWondersInterface.class);
    }

    private static Gson getGson() {
        return new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    }
}
