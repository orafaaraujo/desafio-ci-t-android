package com.ciandt.challengeandroid.worldwondersapp.ui.activities;

import com.ciandt.challengeandroid.worldwondersapp.R;
import com.ciandt.challengeandroid.worldwondersapp.aplication.WorldWondersApplication;
import com.ciandt.challengeandroid.worldwondersapp.rest.WorldWondersAPI;
import com.ciandt.challengeandroid.worldwondersapp.rest.json.WorldWonderJson;
import com.ciandt.challengeandroid.worldwondersapp.rest.json.WorldWonderResponseJson;
import com.ciandt.challengeandroid.worldwondersapp.ui.adapters.WorldWonderAdapter;
import com.ciandt.challengeandroid.worldwondersapp.util.IntentUtil;
import com.ciandt.challengeandroid.worldwondersapp.util.NetworkConnectionUtil;
import com.orhanobut.hawk.Hawk;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import java.util.ArrayList;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by rafael on 16/07/15.
 */
@OptionsMenu(R.menu.menu_main)
@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {


    @ViewById(R.id.main_toolbar)
    protected Toolbar mToolbar;

    @ViewById(R.id.main_swipe_refresh)
    protected SwipeRefreshLayout mSwipeRefreshLayout;

    @ViewById(R.id.main_recycler)
    protected RecyclerView mRecyclerView;

    @AfterViews
    protected void afterViews() {
        setupToolbar();
        setupSwipeToRefresh();
        setupGridRecycler();
        fetchWorldWonders();
    }

    @UiThread
    protected void setupToolbar() {
        setSupportActionBar(mToolbar);
    }

    @UiThread
    protected void setupSwipeToRefresh() {
        mSwipeRefreshLayout.setColorSchemeResources(R.color.primary, R.color.primary_dark);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchWorldWonders();
            }
        });
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(NetworkConnectionUtil.isNetworkConnected(MainActivity.this));
            }
        });
    }

    @UiThread
    protected void setupGridRecycler() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setHasFixedSize(true);
    }

    @Background
    protected void fetchWorldWonders() {
        if (NetworkConnectionUtil.isNetworkConnected(this)) {
            WorldWondersAPI.getServices().getWorldwonders().subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<WorldWonderResponseJson>() {
                        @Override
                        public void onCompleted() {
                            mSwipeRefreshLayout.setRefreshing(false);
                        }

                        @Override
                        public void onError(Throwable e) {
                            mSwipeRefreshLayout.setRefreshing(false);
                            Toast.makeText(MainActivity.this, R.string.server_error, Toast.LENGTH_LONG).show();
                            mRecyclerView.setAdapter(
                                    new WorldWonderAdapter(getApplicationContext(), new ArrayList<WorldWonderJson>()));
                        }

                        @Override
                        public void onNext(WorldWonderResponseJson worldWonderResponseJson) {
                            mRecyclerView.setAdapter(new WorldWonderAdapter(getApplicationContext(),
                                    worldWonderResponseJson.getWorldWonderJsons()));
                        }

                    });
        } else {
            showNoConnectionMessage();
        }
    }

    @UiThread
    protected void showNoConnectionMessage() {
        Toast.makeText(this, R.string.connection_error, Toast.LENGTH_LONG).show();
        mRecyclerView.setAdapter(new WorldWonderAdapter(this, new ArrayList<WorldWonderJson>()));
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @OptionsItem(R.id.menu_setting_logout)
    protected void doLogout() {
        Hawk.remove(WorldWondersApplication.SECURE_TOKEN);
        Hawk.clear();

        IntentUtil.enableIntent(this, LoginActivity_.class);
        LoginActivity_.intent(this).flags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK).start();
        finish();
    }

}