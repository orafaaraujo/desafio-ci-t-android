package com.ciandt.challengeandroid.worldwondersapp.rest;

import com.ciandt.challengeandroid.worldwondersapp.rest.json.LoginResponseJson;
import com.ciandt.challengeandroid.worldwondersapp.rest.json.WorldWonderJson;
import com.ciandt.challengeandroid.worldwondersapp.rest.json.WorldWonderResponseJson;

import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by rafael on 16/07/15.
 */
public interface WorldWondersInterface {

    @GET("/api/v1/login")
    Observable<LoginResponseJson> login(@Query("email") String email, @Query("senha") String password);

    @GET("/api/v1/worldwonders")
    Observable<WorldWonderResponseJson> getWorldwonders();

    @GET("/api/v1/worldwonders/{id}")
    Observable<WorldWonderJson> getWorldwondeById(@Path("id") int id);
}