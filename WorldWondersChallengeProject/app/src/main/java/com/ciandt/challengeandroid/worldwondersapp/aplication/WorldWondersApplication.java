package com.ciandt.challengeandroid.worldwondersapp.aplication;

import com.orhanobut.hawk.Hawk;

import org.androidannotations.annotations.EApplication;

import android.app.Application;

/**
 * Created by rafael on 16/07/15.
 */
@EApplication
public class WorldWondersApplication extends Application {

    public static final String SECURE_TOKEN = "SECURE_TOKEN";

    @Override
    public void onCreate() {
        super.onCreate();

        Hawk.init(this);
    }
}