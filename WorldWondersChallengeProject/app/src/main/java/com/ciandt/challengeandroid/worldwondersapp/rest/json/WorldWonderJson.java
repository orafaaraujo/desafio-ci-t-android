package com.ciandt.challengeandroid.worldwondersapp.rest.json;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rafael on 16/07/15.
 */
public class WorldWonderJson {

    @SerializedName("id")
    private final int mId;

    @SerializedName("name")
    private final String mName;

    @SerializedName("country")
    private final String mCountry;

    @SerializedName("description")
    private final String mDescription;

    @SerializedName("image_url")
    private final String mImage;

    public WorldWonderJson(int id, String name, String country, String description, String image) {
        mId = id;
        mName = name;
        mCountry = country;
        mDescription = description;
        mImage = image;
    }

    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public String getCountry() {
        return mCountry;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getImage() {
        return mImage;
    }
}