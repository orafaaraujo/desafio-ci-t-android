package com.ciandt.challengeandroid.worldwondersapp.ui.adapters;

import com.ciandt.challengeandroid.worldwondersapp.R;
import com.ciandt.challengeandroid.worldwondersapp.rest.json.WorldWonderJson;
import com.ciandt.challengeandroid.worldwondersapp.ui.adapters.viewholders.WorldWonderViewHolder;
import com.ciandt.challengeandroid.worldwondersapp.ui.adapters.viewholders.binder.WorldWonderViewHolderBinder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by rafael on 16/07/15.
 */
public class WorldWonderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;

    private List<WorldWonderJson> mWorldWonders;

    public WorldWonderAdapter(Context context, List<WorldWonderJson> worldWonders) {
        mContext = context;
        mWorldWonders = worldWonders;
    }

    @Override
    public int getItemCount() {
        return mWorldWonders.size();
    }

    public WorldWonderJson getItem(int i) {
        return mWorldWonders.get(i);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        return new WorldWonderViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.world_wonder_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        WorldWonderViewHolderBinder.bindData(mContext, (WorldWonderViewHolder) holder, getItem(position));
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

}