package com.ciandt.challengeandroid.worldwondersapp.util;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;

/**
 * Created by rafael on 16/07/15.
 */
public class IntentUtil {

    /**
     * Método responsável por habilitar Activity no sistema.
     */
    public static void enableIntent(Context context, Class<?> cls) {
        context.getPackageManager().setComponentEnabledSetting(new ComponentName(context, cls),
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
    }
}