package com.ciandt.challengeandroid.worldwondersapp.ui.adapters.viewholders.binder;

import com.bumptech.glide.Glide;
import com.ciandt.challengeandroid.worldwondersapp.R;
import com.ciandt.challengeandroid.worldwondersapp.rest.json.WorldWonderJson;
import com.ciandt.challengeandroid.worldwondersapp.ui.adapters.viewholders.WorldWonderViewHolder;

import android.content.Context;

/**
 * Created by rafael on 16/07/15.
 */
public class WorldWonderViewHolderBinder {

    public static void bindData(final Context context, final WorldWonderViewHolder holder,
            final WorldWonderJson worldWonder) {

        holder.mName.setText(worldWonder.getName());
        holder.mCountry.setText(worldWonder.getCountry());
        holder.mDescription.setText(worldWonder.getDescription());

        Glide.with(context).load(worldWonder.getImage()).crossFade().placeholder(R.drawable.ic_image_photo)
                .error(R.drawable.ic_image_photo).into(holder.mImageView);
    }
}