package com.ciandt.challengeandroid.worldwondersapp.ui.activities;

import com.ciandt.challengeandroid.worldwondersapp.aplication.WorldWondersApplication;
import com.ciandt.challengeandroid.worldwondersapp.util.IntentUtil;
import com.orhanobut.hawk.Hawk;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by rafael on 16/07/15.
 */
public class StartActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent;
        if (Hawk.contains(WorldWondersApplication.SECURE_TOKEN)) {
            IntentUtil.enableIntent(this, MainActivity_.class);
            intent = MainActivity_.intent(this).get();
        } else {
            IntentUtil.enableIntent(this, LoginActivity_.class);
            intent = LoginActivity_.intent(this).get();
        }

        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }
}