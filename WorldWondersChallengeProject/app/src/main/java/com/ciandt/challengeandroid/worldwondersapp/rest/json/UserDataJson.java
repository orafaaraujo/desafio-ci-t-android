package com.ciandt.challengeandroid.worldwondersapp.rest.json;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rafael on 16/07/15.
 */
public class UserDataJson {

    @SerializedName("token_auth")
    private final String mToken;

    @SerializedName("nome")
    private final String mName;

    @SerializedName("habilitado")
    private final boolean mEnable;

    public UserDataJson(String token, String name, boolean enable) {
        mToken = token;
        mName = name;
        mEnable = enable;
    }

    public String getToken() {
        return mToken;
    }

    public String getName() {
        return mName;
    }

    public boolean isEnable() {
        return mEnable;
    }
}