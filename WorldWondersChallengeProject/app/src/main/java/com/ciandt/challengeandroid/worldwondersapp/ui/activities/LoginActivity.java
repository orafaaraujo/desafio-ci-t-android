package com.ciandt.challengeandroid.worldwondersapp.ui.activities;

import com.ciandt.challengeandroid.worldwondersapp.R;
import com.ciandt.challengeandroid.worldwondersapp.aplication.WorldWondersApplication;
import com.ciandt.challengeandroid.worldwondersapp.policy.EmailPolicy;
import com.ciandt.challengeandroid.worldwondersapp.policy.PasswordPolicy;
import com.ciandt.challengeandroid.worldwondersapp.rest.WorldWondersAPI;
import com.ciandt.challengeandroid.worldwondersapp.rest.json.LoginResponseJson;
import com.ciandt.challengeandroid.worldwondersapp.util.IntentUtil;
import com.ciandt.challengeandroid.worldwondersapp.util.NetworkConnectionUtil;
import com.orhanobut.hawk.Hawk;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.animation.Animator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.StringRes;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.ViewTreeObserver;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@EActivity(R.layout.activity_login)
public class LoginActivity extends AppCompatActivity {

    @ViewById(R.id.login_image)
    protected ImageView mLoginImage;

    @ViewById(R.id.login_inputlayout_email)
    protected TextInputLayout mTextInputLayoutEmail;

    @ViewById(R.id.login_email)
    protected EditText mEditEmail;

    @ViewById(R.id.login_inputlayout_password)
    protected TextInputLayout mTextInputLayoutPassword;

    @ViewById(R.id.login_password)
    protected EditText mEditPassword;

    @ViewById(R.id.login_enter)
    protected Button mButtonEnter;

    private ProgressDialog mLoadingDialog;

    @AfterViews
    protected void afterViews() {
        startAnimation();
    }

    /**
     * Pequena animação de inicio de tela.
     */
    @UiThread
    protected void startAnimation() {
        final int mediumAnimTime = getResources().getInteger(android.R.integer.config_mediumAnimTime);
        final int longAnimTime = getResources().getInteger(android.R.integer.config_longAnimTime);

        mLoginImage.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

            @Override
            public boolean onPreDraw() {
                mLoginImage.getViewTreeObserver().removeOnPreDrawListener(this);

                setViewAlphaZero();

                mLoginImage.setScaleY(0);
                mLoginImage.setScaleX(0);
                mLoginImage.animate().scaleY(1).scaleX(1).setDuration(longAnimTime).setStartDelay(mediumAnimTime)
                        .setInterpolator(new OvershootInterpolator(2f)).setListener(imageAnimationListener()).start();

                return false;
            }
        });
    }

    @UiThread
    protected void setViewAlphaZero() {
        ViewCompat.setAlpha(mEditEmail, 0);
        ViewCompat.setAlpha(mEditPassword, 0);
        ViewCompat.setAlpha(mButtonEnter, 0);
        ViewCompat.setAlpha(mTextInputLayoutEmail, 0);
        ViewCompat.setAlpha(mTextInputLayoutPassword, 0);
    }

    @UiThread
    protected void setViewAlphaVisible() {
        final int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        ViewCompat.animate(mEditEmail).alpha(1f).setDuration(shortAnimTime);
        ViewCompat.animate(mEditPassword).alpha(1f).setDuration(shortAnimTime);
        ViewCompat.animate(mButtonEnter).alpha(1f).setDuration(shortAnimTime);
        ViewCompat.animate(mTextInputLayoutEmail).alpha(1f).setDuration(shortAnimTime);
        ViewCompat.animate(mTextInputLayoutPassword).alpha(1f).setDuration(shortAnimTime);
    }

    private Animator.AnimatorListener imageAnimationListener() {
        return new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                setViewAlphaVisible();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        };
    }

    @Click(R.id.login_enter)
    protected void onEnterButtonClick() {
        final boolean emailIsValid = validateEmail();
        final boolean passwordIsValid = validatePassword();
        if (emailIsValid && passwordIsValid) {
            doLogin();
        }
    }

    @Background
    protected void doLogin() {
        if (NetworkConnectionUtil.isNetworkConnected(this)) {
            startProgressDialog();
            WorldWondersAPI.getServices().login(mEditEmail.getText().toString(), mEditPassword.getText().toString())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<LoginResponseJson>() {
                        @Override
                        public void onCompleted() {
                            stopLoadingDialog();
                        }

                        @Override
                        public void onError(Throwable e) {
                            stopLoadingDialog();
                            Toast.makeText(LoginActivity.this, R.string.server_error, Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onNext(LoginResponseJson json) {
                            if (json.getCode() > 0 && json.getData() == null) {
                                if (!TextUtils.isEmpty(json.getMessage())) {
                                    Toast.makeText(LoginActivity.this, json.getMessage(), Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(LoginActivity.this, R.string.server_error, Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Hawk.put(WorldWondersApplication.SECURE_TOKEN, json.getData().getToken());
                                goToMain();
                            }
                        }
                    });
        } else {
            showNoConnectionMessage();
        }
    }

    @UiThread
    protected void showNoConnectionMessage() {
        Toast.makeText(this, R.string.connection_error, Toast.LENGTH_LONG).show();
    }

    /**
     * Valida o campo e-mail.
     */
    private boolean validateEmail() {
        if (EmailPolicy.isValid(mEditEmail.getText().toString())) {
            return true;
        }
        setError(mEditEmail, getString(R.string.login_edittext_empty));
        return false;
    }

    /**
     * Valida o campo senha.
     */
    private boolean validatePassword() {
        if (PasswordPolicy.isValid(mEditPassword.getText().toString())) {
            return true;
        }
        setError(mEditPassword, getString(R.string.login_edittext_empty));
        return false;
    }

    /**
     * Seta erros nos editTexts.
     *
     * @param editText     Edittext que deverá ser notificado com o erro.
     * @param errorMessage Mensagem de erro do usuário.
     */
    @UiThread
    protected void setError(EditText editText, @StringRes String errorMessage) {
        editText.setError(errorMessage);
        editText.requestFocus();
    }

    /**
     * Inicia dialog.
     */
    @UiThread
    protected void startProgressDialog() {
        mLoadingDialog = new ProgressDialog(this);
        mLoadingDialog.setIndeterminate(true);
        mLoadingDialog.setMessage(getString(R.string.waiting));
        mLoadingDialog.setCancelable(false);
        mLoadingDialog.show();
    }

    /**
     * Cancela dialog.
     */
    @UiThread
    protected void stopLoadingDialog() {
        if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
            mLoadingDialog.dismiss();
        }
    }

    @UiThread
    protected void goToMain() {
        IntentUtil.enableIntent(this, MainActivity_.class);
        MainActivity_.intent(this).flags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK).start();
        finish();
    }

}
